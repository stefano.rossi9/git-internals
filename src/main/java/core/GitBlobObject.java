package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
    private String repoPath;
    private String hash;

    public GitBlobObject(String repoPath, String hash) {
        this.repoPath = repoPath;
        this.hash = hash;
    }

    public String getType() throws IOException{
        InflaterInputStream inflater = new InflaterInputStream(new FileInputStream(repoPath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2) ));
        String result = "";
        while(inflater.available() == 1) {
            char byteRead = (char)inflater.read();
            if(byteRead == ' ') break;
            result += byteRead;
        }
        inflater.close();
        return result;
    }

    public String getContent() throws IOException {
        InflaterInputStream inflater = new InflaterInputStream(new FileInputStream(repoPath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2) ));
        String result = "";
        while(inflater.available() == 1 && inflater.read() != 0) {
        }

        while(inflater.available() == 1) {
            int byteRead = inflater.read();

            if(byteRead > 0)
            result += (char)byteRead;
        }

        inflater.close();
        return result;
    }
}
