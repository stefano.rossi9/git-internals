package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {
    private String repoPath;
    public GitRepository(String repoPath) {
        this.repoPath = repoPath;
    }

    public String getHeadRef() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(this.repoPath + "/HEAD"));
        return reader.readLine().replace("ref: ", "");
    }

    public String getRefHash(String ref) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(this.repoPath + "/" + ref));
        return reader.readLine();
    }
}
