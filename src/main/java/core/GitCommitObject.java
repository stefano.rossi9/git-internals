package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitCommitObject {
    private final String repoPath;
    private final String hash;

    public GitCommitObject(String repoPath, String commitHash) {
        this.repoPath = repoPath;
        this.hash = commitHash;
    }

    public String getHash() {
        return this.hash;
    }

    public String getTreeHash() throws IOException {
        BufferedReader reader = getCommitReader();
        String result = reader.readLine().replaceFirst(".+tree ", "");
        reader.close();
        return result;
    }

    public String getParentHash() throws IOException {
        BufferedReader reader = getCommitReader();
        reader.readLine();
        String result = reader.readLine().replaceFirst(".*parent ", "");
        reader.close();
        return result;
    }

    public String getAuthor() throws IOException {
        BufferedReader reader = getCommitReader();
        reader.readLine();
        reader.readLine();
        String author = reader.readLine().replaceFirst(".*author ", "");
        author = author.substring(0, author.indexOf(">")+1);
        reader.close();
        return author;
    }

    private BufferedReader getCommitReader() throws FileNotFoundException {
        return new BufferedReader(
                    new InputStreamReader(
                            new InflaterInputStream(
                                    new FileInputStream(repoPath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2) )
                            )
                    )
            );
    }
}
